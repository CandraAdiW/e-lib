import { z } from "zod";

export const createSchema = z.object({
    body: z.object({
        username: z.string({
            required_error: "name Wajib Diisi",
        }).email(),
        password: z.string({
            required_error: "Password Wajib Diisi",
        }),
        is_active: z.boolean().optional()
    }),
});

export const params = z.object({
    id: z.string(),
});

export const updateSchema = z.object({
    params,
    body: z
        .object({
            username: z.string(),
            password: z.string(),
            is_active: z.boolean(),
        })
        .partial(),
});

export const filterQuery = z.object({
    limit: z.number().default(1),
    page: z.number().default(10),
    search : z.string().optional()
});

export type ParamsInput = z.TypeOf<typeof params>;
export type FilterQueryInput = z.TypeOf<typeof filterQuery>;
export type CreateInput = z.TypeOf<typeof createSchema>["body"];
export type UpdateInput = z.TypeOf<typeof updateSchema>;