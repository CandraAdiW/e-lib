import * as dotenv from "dotenv";
import { Sequelize, DataTypes } from "sequelize";
dotenv.config();

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PWD, {
    port : Number(process.env.DB_PORT),
    host: process.env.DB_HOST,
    dialect: 'mariadb', /* one of 'mysql' | 'postgres' | 'sqlite' | 'mariadb' | 'mssql' | 'db2' | 'snowflake' | 'oracle' */
        // Choose one of the logging options
        //logging: console.log,                  // Default, displays the first parameter of the log function call
        logging: (...msg) => console.log(msg), // Displays all log function call parameters
        // logging: false,                        // Disables logging
        // logging: msg => logger.debug(msg),     // Use custom logger (e.g. Winston or Bunyan), displays the first parameter
        // logging: logger.debug.bind(logger)     // Alternative way to use custom logger, displays all messages
});

async function connectDB() {
    try {
        await sequelize.authenticate();
        console.log("✅ Connection has been established successfully.");
    } catch (error) {
        console.error("Unable to connect to the database:", error);
    }
}

export { connectDB, sequelize, Sequelize, DataTypes };
