import {spawn} from "child_process";
import path from "path";
import { UploadFile } from "../_utils/_utils.interface";

export const prosesThumbnails = async (file_info: UploadFile, page_thumbnail : number[]) =>{
    let hasil = [];
    for (let halaman of page_thumbnail) {
        let nama_file = `thumbnail-${Date.now()}.png`;
        let path_thumbnail = `${file_info.destination}/${nama_file}`;
        let hasilCmd = await jalankanCmd(`gs  -sDEVICE=jpeg -o '${path.resolve(path_thumbnail)}'  -dFirstPage=${halaman}  -dLastPage=${halaman}  -dJPEGQ=100  -r75x75  '${path.resolve(file_info.path)}'`);
        if (hasilCmd == 0) {
            hasil.push({ nama_file: nama_file, path: path_thumbnail });
        } else {
            throw new Error("Terjadi kesalahan pada proses thumbnail");
        }
    }
    return hasil;
}

function jalankanCmd(command : string) {
    let p = spawn(command,{ shell: true });
    return new Promise((resolveFunc) => {
        p.stdout.on("data", (x) => {
            process.stdout.write(x.toString());
        });
        p.stderr.on("data", (x) => {
            process.stderr.write(x.toString());
        });
        p.on("exit", (code) => {
            resolveFunc(code);
        });
    });
}
