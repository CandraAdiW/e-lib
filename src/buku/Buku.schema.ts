import { z } from "zod";

export const createSchema = z.object({
    body: z.object({
        id_buku : z.number({
            required_error: "Buku harus diisi",
        }),
        jumlah: z.number({
            required_error: "Jumlah harus diisi",
        }),
        status: z.number({
            required_error: "Status harus diisi",
        }),
        deskripsi: z.string()
    }),
});

export const params = z.object({
    id: z.string(),
});

export const updateSchema = z.object({
    params,
    body: z
        .object({
            id_buku: z.number({
                required_error: "Buku harus diisi",
            }),
            jumlah: z.number({
                required_error: "Jumlah harus diisi",
            }),
            status: z.number({
                required_error: "Status harus diisi",
            }),
            deskripsi: z.string()
        })
        .partial(),
});

export const filterQuery = z.object({
    limit: z.number().default(1),
    page: z.number().default(10),
    search : z.string().optional()
});

export type ParamsInput = z.TypeOf<typeof params>;
export type FilterQueryInput = z.TypeOf<typeof filterQuery>;
export type CreateInput = z.TypeOf<typeof createSchema>["body"];
export type UpdateInput = z.TypeOf<typeof updateSchema>;