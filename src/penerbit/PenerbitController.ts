import { Request, Response } from "express";
import Model from "./PenerbitModel";
import {CreateInput,FilterQueryInput,ParamsInput,UpdateInput} from "./Penerbit.schema";
import { Op } from 'sequelize';

export const createController = async (
    req: Request<{}, {}, CreateInput>,
    res: Response
) => {
    try {
        const { nama } = req.body;
        const data = await Model.create({
            nama
        });

        res.status(201).json({
            status: "success",
            data: data,
        });
    } catch (error: any) {
        if (error.name === "SequelizeUniqueConstraintError") {
            return res.status(409).json({
                status: "failed",
                message: "Note with that title already exists",
            });
        }

        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};

export const updateController = async (
    req: Request<UpdateInput["params"], {}, UpdateInput["body"]>,
    res: Response
) => {
    try {
        const result = await Model.update(
            { ...req.body, updated_date: Date.now() },
            {
                where: {
                    id: req.params.id,
                },
            }
        );

        if (result[0] === 0) {
            return res.status(404).json({
                status: "fail",
                message: "Note with that ID not found",
            });
        }

        const data = await Model.findByPk(req.params.id);

        res.status(200).json({
            status: "success",
            data: data,
        });
    } catch (error: any) {
        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};

export const findController = async (
    req: Request<ParamsInput>,
    res: Response
) => {
    try {
        const data = await Model.findByPk(req.params.id);

        if (!data) {
            return res.status(404).json({
                status: "fail",
                message: "Note with that ID not found",
            });
        }

        res.status(200).json({
            status: "success",
            data: data,
        });
    } catch (error: any) {
        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};

export const findAllController = async (
    req: Request<{}, {}, {}, FilterQueryInput>,
    res: Response
) => {
    try {
        const page = req.query.page || 1;
        const limit = parseInt(req.query.limit as unknown as string,10) || 10;
        const skip = (page - 1) * limit;

        const data = await Model.findAll({ limit, offset: skip ,where :{
                nama : { [Op.like] : `%${req.query.search ? req.query.search : ''}%`}
            },
            raw : true
        });
        res.status(200).json({
            status: "success",
            results: data.length,
            data,
        });
    } catch (error: any) {
        console.log("error", error);
        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};

export const deleteController = async (
    req: Request<ParamsInput>,
    res: Response
) => {
    try {
        const result = await Model.destroy({
            where: { id: req.params.id },
            force: true,
        });

        if (result === 0) {
            return res.status(404).json({
                status: "fail",
                message: "Note with that ID not found",
            });
        }

        res.status(204).json();
    } catch (error: any) {
        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};