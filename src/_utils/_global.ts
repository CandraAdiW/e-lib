import { Secret, decode, verify, sign } from 'jsonwebtoken';

export function validateToken(token: string): Promise<any> {
    const verifyOptions = {
        algorithms: 'HS512',
    };

    return new Promise((resolve, reject) => {
        verify(token, process.env.TOKEN_KEY, (error, decoded) => {
            if (error) return reject(error);

            resolve(decoded);
        })
    });
}