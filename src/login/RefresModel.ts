import { connectDB, sequelize, Sequelize, DataTypes } from "../_utils/_utils";

const refresh = sequelize.define("refresh_token", {
    user_id: {
        type: DataTypes.NUMBER,
        primaryKey: true
    },
    refresh_token: {
        type: DataTypes.STRING
    },
    expired_date: {
        type: DataTypes.DATE
    }
}, {
    freezeTableName: true,
    timestamps : false
});


export default refresh;