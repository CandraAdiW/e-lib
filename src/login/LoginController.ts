import { Request, Response } from "express";
import Model from "../user/UserModel";
import RefreshModel from "./RefresModel";
import { Input, refreshInput } from "./Login.schema";
import * as argon2 from "argon2";
import crypto from "crypto";
import { Secret, decode, verify ,sign} from 'jsonwebtoken';
import { Op } from 'sequelize';

export const loginUser = async (
    req: Request<{}, {}, Input>,
    res: Response
) => {
    try {
        let { username, password } = req.body;
        let hasilUser = await Model.findOne({ where: { username: username } ,raw:true});
        if (!hasilUser){
            throw new Error("Username atau Password salah");
        }
        let hasilVerify = await argon2.verify(hasilUser["password"],password);
        if (!hasilVerify){
            throw new Error("Username atau Password salah");
        }
        const token = sign(
            { user_id: hasilUser["id"], email: hasilUser["username"] },
            process.env.TOKEN_KEY,
            {
                algorithm: 'HS512',
                expiresIn: process.env.EXPIRED_KEY,
            }
        );

        let refresh_token = crypto.randomUUID();
        let hasilRefresh = await RefreshModel.findOrCreate({ where: { user_id: hasilUser["id"] }, defaults: { user_id: hasilUser["id"], refresh_token: refresh_token, expired_date: tambahHari(3)} })
        if (!hasilRefresh){
            throw new Error("Refresh token gagal");
        }
        res.status(200).json({
            status: "success",
            data: {
                token: token,
                refresh_token : refresh_token,
            },
        });
    } catch (error: any) {
        if (error.name === "SequelizeUniqueConstraintError") {
            return res.status(409).json({
                status: "failed",
                message: "Note with that title already exists",
            });
        }

        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};


export const refreshToken = async (
    req: Request<{}, {}, refreshInput>,
    res: Response
) => {
    try {
        let { refresh_token } = req.body;
        
        let cariRefreshToken = await RefreshModel.findOne({ where: { refresh_token: refresh_token, expired_date: { [Op.gt]: Date.now() } }, raw: true });
        if (!cariRefreshToken){
            throw new Error('Refresh token tidak valid');
        }
        let hasilUser = await Model.findOne({ where: { id: cariRefreshToken["user_id"] }, raw: true });
        if (!hasilUser) {
            throw new Error("Refresh token tidak valid");
        }
        const token = sign(
            { user_id: hasilUser["id"], email: hasilUser["username"] },
            process.env.TOKEN_KEY,
            {
                algorithm: 'HS512',
                expiresIn: process.env.EXPIRED_KEY,
            }
        );

        let refresh_tokens = crypto.randomUUID();
        let hasilRefresh = await RefreshModel.findOrCreate({ where: { user_id: hasilUser["id"] }, defaults: { user_id: hasilUser["id"], refresh_token: refresh_tokens, expired_date: tambahHari(3) } })
        if (!hasilRefresh) {
            throw new Error("Refresh token gagal");
        }
        res.status(200).json({
            status: "success",
            data: {
                token: token,
                refresh_token: refresh_tokens,
            },
        });
    } catch (error: any) {
        if (error.name === "SequelizeUniqueConstraintError") {
            return res.status(409).json({
                status: "failed",
                message: "Note with that title already exists",
            });
        }

        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
}



function tambahHari(hari : number = 2){
    let hariIni = new Date();
    let tglHariIni = hariIni.getDate();
    return hariIni.setDate(tglHariIni+hari);
}