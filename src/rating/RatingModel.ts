import { connectDB, sequelize, Sequelize, DataTypes } from "../_utils/_utils";
import UserModel from "../user/UserModel";

const rating = sequelize.define("rating", {
    id: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement : true
    },
    id_buku: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    id_user: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    rating: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    createdAt: {
        field: 'created_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
    updatedAt: {
        field: 'updated_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
},{
    freezeTableName: true
});

UserModel.hasMany(rating,{foreignKey : "id_user"});
rating.belongsTo(UserModel,{targetKey : "id",foreignKey:"id_user"});

export default rating;