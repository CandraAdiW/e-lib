import { connectDB, sequelize, Sequelize, DataTypes } from "../_utils/_utils";


const user = sequelize.define("user", {
    id: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement : true
    },
    username: {
        type: DataTypes.STRING(200),
        allowNull: false,
        unique: true,
    },
    password: {
        type: DataTypes.STRING(300),
        allowNull: false,
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false,
    },
    deleted_date: {
        type: DataTypes.DATE,
        allowNull: true,
    },
    createdAt: {
        field: 'created_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
    updatedAt: {
        field: 'updated_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
}, 
{
        freezeTableName: true
});

export default user;