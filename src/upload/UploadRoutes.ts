import express from "express";
import { createController } from "./UploadController";
import { createSchema } from "./Upload.schema";
import { validate } from "../middleware/validate";
import multer from "multer";
import path from "path";
import * as fs from 'fs';
import { authorize } from "../middleware/Auth";

const tanggalSekarang = Date.now();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        let directory = `public/uploads/${tanggalSekarang}`;
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true })
        }
        cb(null, directory );
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + tanggalSekarang + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage }).single("file");
const router = express.Router();
//validate(createSchema),
router.route("/").post(authorize(), upload,createController);

export default router;