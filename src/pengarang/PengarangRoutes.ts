import express from "express";
import { validate } from "../middleware/validate";
import { createController, deleteController, findAllController, findController, updateController } from "./PengarangController";
import { createSchema, updateSchema } from "./Pengarang.schema";
import { authorize } from "../middleware/Auth";

const router = express.Router();

router
    .route("/")
    .get(authorize(), findAllController)
    .post(authorize(), validate(createSchema), createController);
router
    .route("/:id")
    .get(authorize(), findController)
    .patch(authorize(), validate(updateSchema), updateController)
    .delete(authorize(), deleteController);

export default router;