import { connectDB, sequelize, Sequelize, DataTypes } from "../_utils/_utils";

const rating = sequelize.define("pinjam", {
    id: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement : true
    },
    id_buku: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    id_user: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    jumlah: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    denda: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    tanggal_pinjam: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    tanggal_kembali: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    nomor_peminjaman : {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
    },
    deskripsi_sebelum_pinjam: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    deskripsi_setelah_kembali: {
        type: DataTypes.STRING,
    },
    createdAt: {
        field: 'created_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
    updatedAt: {
        field: 'updated_date',
        type: DataTypes.DATE,
        allowNull: true,
    },

    //
},{
    freezeTableName: true
});


export default rating;