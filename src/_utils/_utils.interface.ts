export interface ResponseData {
    data : any[],
    message : string,
    status : boolean, 
    response_code : number
}

export interface UploadFile {
    fieldname: string,
    originalname: string,
    encoding: string,
    mimetype: string,
    destination: string,
    filename: string,
    path: string,
    size: number
}

