import { z } from "zod";

export const loginSchema = z.object({
    body: z.object({
        username: z.string({
            required_error: "name Wajib Diisi",
        }).email(),
        password: z.string({
            required_error: "Password Wajib Diisi",
        })
    }),
});

export const refreshSchema = z.object({
    body: z.object({
        refresh_token: z.string({
            required_error: "Refresh Wajib Diisi",
        })
    }),
});

export type Input = z.TypeOf<typeof loginSchema>["body"];
export type refreshInput = z.TypeOf<typeof refreshSchema>["body"];