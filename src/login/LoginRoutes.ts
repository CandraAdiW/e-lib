import express from "express";
import { validate } from "../middleware/validate";
import { loginUser, refreshToken } from "./LoginController";
import { loginSchema, refreshSchema } from "./Login.schema";

const router = express.Router();

router
    .route("/")
    .post(validate(loginSchema), loginUser);

router
    .route("/refresh")
    .post(validate(refreshSchema), refreshToken);

export default router;