import { Request, Response, NextFunction } from "express";
import { validateToken } from "../_utils/_global";

export const authorize = () => async (req: Request, res: Response, next: NextFunction) => {
    try {
        let jwt = req.headers.authorization;

        // verify request has token
        if (!jwt) {
            return res.status(401).json({ message: 'Token Invalid ' });
        }

        // remove Bearer if using Bearer Authorization mechanism
        if (jwt.toLowerCase().startsWith('bearer')) {
            jwt = jwt.slice('bearer'.length).trim();
        }

        // verify token hasn't expired yet
        const decodedToken = await validateToken(jwt);
        req["userData"] = decodedToken;

        next();
    } catch (error) {
        if (error.name === 'TokenExpiredError') {
            res.status(401).json({ message: 'Token Kadaluarsa' });
            return;
        }

        res.status(500).json({ message: 'Autentikasi User Gagal' });
    }
};