import { Request, Response } from "express";
import { prosesThumbnails } from "./ThumbailProses";

export const createController = async (
    req: Request,
    res: Response
) => {
    try {
        let hasil  = [];
        if (req.body.is_generate_thumbnail){
            hasil = await prosesThumbnails(req.file, req.body.page_thumbnail ? JSON.parse(req.body.page_thumbnail) : [1]);
        }
        res.status(200).json({
            status: "success",
            data: {
                file_upload: req.file,
                thumbnail: hasil
            },
        });
    } catch (error: any) {
        if (error.name === "SequelizeUniqueConstraintError") {
            return res.status(409).json({
                status: "failed",
                message: "Note with that title already exists",
            });
        }

        res.status(500).json({
            status: "error",
            message: error.message,
        });
    }
};
