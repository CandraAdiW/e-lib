import express, { NextFunction, Request, Response } from "express";
import cors from 'cors';
import helmet from "helmet";
import * as dotenv from "dotenv"; 
import { ResponseData } from "./src/_utils/_utils.interface";
import UserRouter from "./src/user/UserRoutes";
import RatingRouter from "./src/rating/RatingRoutes";
import KategoriRouter from "./src/kategori/KategoriRoutes";
import SubKategoriRouter from "./src/subKategori/SubKategoriRoutes";
import FromatRouter from "./src/format/FormatRoutes";
import PenerbitRouter from "./src/penerbit/PenerbitRoutes";
import PengarangRouter from "./src/pengarang/PengarangRoutes";
import PinjamRouter from "./src/pinjam/PinjamRoutes";
import StokRouter from "./src/stok/StokRoutes";
import BukuRouter from "./src/buku/BukuRoutes";
import UploadRouter from "./src/upload/UploadRoutes";
import LoginRouter from "./src/login/LoginRoutes";



dotenv.config();

const app = express();

const allowedOrigins = ['http://localhost:7000'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};

app.use(helmet());
app.use(cors(options));
app.use(express.json())

const APP_PORT: number = parseInt(process.env.APP_PORT as string, 10);

app.use("/api/login", LoginRouter);
app.use("/api/user", UserRouter);
app.use("/api/rating", RatingRouter);
app.use("/api/kategori", KategoriRouter);
app.use("/api/sub-kategori", SubKategoriRouter);
app.use("/api/format", FromatRouter);
app.use("/api/penerbit", PenerbitRouter);
app.use("/api/pengarang",PengarangRouter);
app.use("/api/pinjam", PinjamRouter);
app.use("/api/stok", StokRouter);
app.use("/api/buku", BukuRouter);
app.use("/api/upload", UploadRouter);
//app.use("/api/login", require("./src/auth/Login"));

app.get("/api/healthchecker", (req: Request, res: Response) => {
  let data: ResponseData = {
    data : [],
    response_code: 200,
    status: true,
    message: "Api Up and Run!",
  }
  res.status(data.response_code).json(data);
})

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  let data: ResponseData = {
    data: [],
    response_code: 200,
    message: "",
    status: false
  }
  console.log("err", err);
  data["message"] = err.message;
  res.status(data.response_code).json(data);
});

app.listen(APP_PORT, () => {
  return console.log(`Express is listening at http://localhost:${APP_PORT}`);
});
