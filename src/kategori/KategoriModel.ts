import { connectDB, sequelize, Sequelize, DataTypes } from "../_utils/_utils";

const rating = sequelize.define("kategori", {
    id: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement : true
    },
    nama: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    createdAt: {
        field: 'created_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
    updatedAt: {
        field: 'updated_date',
        type: DataTypes.DATE,
        allowNull: true,
    },
},{
    freezeTableName: true
});


export default rating;