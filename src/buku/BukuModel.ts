import { connectDB, sequelize, Sequelize, DataTypes } from "../_utils/_utils";

const rating = sequelize.define("stok", {
    id: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement : true
    },
    id_buku: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    jumlah: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    status: {
        type: DataTypes.NUMBER,
        allowNull: false,
    },
    deskripsi: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    created_date: {
        type: DataTypes.DATE,
        allowNull: false,
    }
},{
    freezeTableName: true,
    timestamps: false
});


export default rating;