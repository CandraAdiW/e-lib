import { z } from "zod";

export const createSchema = z.object({
    body: z.object({
        id_buku : z.number({
            required_error: "Buku harus diisi",
        }),
        id_user: z.number({
            required_error: "User harus diisi",
        }),
        jumlah: z.number({
            required_error: "Jumlah harus diisi",
        }),
        tanggal_pinjam: z.string().datetime(),
        tanggal_kembali: z.string().datetime().optional(),
        denda : z.number().optional(),
        deskripsi_sebelum_pinjam: z.string({
            required_error: "Deskripsi Sebelum harus diisi",
        }),
        deskripsi_setelah_kembali : z.string().optional()
    }),
});

export const params = z.object({
    id: z.string(),
});

export const updateSchema = z.object({
    params,
    body: z
        .object({
            // id_buku: z.number({
            //     required_error: "Buku harus diisi",
            // }),
            // id_user: z.number({
            //     required_error: "User harus diisi",
            // }),
            // jumlah: z.number({
            //     required_error: "Jumlah harus diisi",
            // }),
            tanggal_kembali: z.string().datetime(),
            denda: z.number({
                required_error: "Denda harus diisi",
            }),
            deskripsi_setelah_kembali: z.string({
                required_error: "Deskripsi Kembali harus diisi",
            })
        })
        .partial(),
});

export const filterQuery = z.object({
    limit: z.number().default(1),
    page: z.number().default(10),
    search : z.string().optional()
});

export type ParamsInput = z.TypeOf<typeof params>;
export type FilterQueryInput = z.TypeOf<typeof filterQuery>;
export type CreateInput = z.TypeOf<typeof createSchema>["body"];
export type UpdateInput = z.TypeOf<typeof updateSchema>;