import { z } from "zod";

const MAX_FILE_SIZE = 500000; // Number of bytes in a megabyte.
const ACCEPTED_IMAGE_TYPES = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];

export const createSchema = z.object({
    body: z.object({
        is_generate_thumbnail : z.boolean().optional(),
        page_thumbnail : z.array(z.number()).optional(),
        file: z.any()
            .refine((file) => file?.length === 0, "Image is required.") // if no file file?.length === 0, if file file?.length === 1
            .refine((file) => file?.[0]?.size >= MAX_FILE_SIZE, `Max file size is 5MB.`) // this should be greater than or equals (>=) not less that or equals (<=)
            .refine(
                (file) => ACCEPTED_IMAGE_TYPES.includes(file?.[0]?.type),
                ".jpg, .jpeg, .png and .pdf file are accepted."
            ),
    }),
});

export type CreateInput = z.TypeOf<typeof createSchema>["body"];
